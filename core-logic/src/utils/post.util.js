const axios = require('axios');
const { timeStamp } = require('./timeStamp.util');

module.exports.post = async (url, data, options) => {
    const init = timeStamp();
    const result = await axios.post(url, data, options);
    return {
        init,
        end: timeStamp(),
        payload: !!result.data ? result.data : null,
        statusMessage: !!result.statusText ? result.statusText : "OK",
        statusCode: !!result.status ? result.status : null};
};


