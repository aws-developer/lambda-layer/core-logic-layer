module.exports.headers = () => {
    return {
        'Access-Control-Expose-Headers': 'Authorization, Content-Type',
        'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,PATCH',
        'Access-Control-Allow-Origin': `${process.env.domain}`,
        'Access-Control-Allow-Credentials': 'true',
        'Cache-Control': 'no-store',
        'Strict-Transport-Security': 'max-age=31536000; includeSubDomains; preload',
        'Content-Type' : 'application/json',
        'X-Content-Type-Options': 'nosniff',
        'X-Frame-Options': 'DENY'};
};

