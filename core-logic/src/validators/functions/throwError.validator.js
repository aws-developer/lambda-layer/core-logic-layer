const { timeStamp } = require('../../utils/timeStamp.util');

const Validator = require('jsonschema').Validator;
const v = new Validator();

const errorTransform = res => {
    return {
        name: res.name,
        message: res.message,
        argument: res.argument,
        stack: res.stack};
};

module.exports.schemaValidator = (data, schema, msn) => {
    const validationResult = v.validate(data, schema, { setDefaults: true, clean: true});

    if(!validationResult.valid){

        const errors = validationResult.errors.forEach(res => errorTransform(res));

        const error = new Error(msn);
        error.time = timeStamp();
        error.data = data;
        error.validation = errors.length > 0 ? errors : validationResult.errors;
        throw error;
    }
};


