const { timeStamp } = require('../../utils/timeStamp.util');

module.exports.statusPetition = res => {
    const statusOkMin = 200;
    const statusOkMax = 200;
    if(res.statusCode < statusOkMin || res.statusCode > statusOkMax){
        const error = new Error(`Http status code ${res.statusCode}: ${res.statusMessage}`);
        error.time = timeStamp();
        error.data = {message: `${res.statusMessage}`, response: res.payload};
        error.validation = null;
        throw error;
    }
};

