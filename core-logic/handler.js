const { post } = require('./src/utils/post.util');
const { ResponseBusiness } = require('./src/business/response.business');
const { schemaValidator } = require('./src/validators/functions/throwError.validator');
const { statusPetition } = require('./src/validators/functions/statusPetition.validator');

const responseHttp = (statusCode, statusMessage, payload) => new ResponseBusiness(statusCode, statusMessage, payload).getResponse();
const postRequest = async (url, data, options) => post(url, data, options);
const validator = (data, schema, msn) => schemaValidator(data, schema, msn);
const validatorPetition = (res,body) => statusPetition(res,body);

module.exports = {
  responseHttp,
  postRequest,
  validator,
  validatorPetition};


